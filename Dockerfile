FROM node:12.19.0-alpine

# # set working directory
WORKDIR /app

# add '/usr/src/app/node_modules/.bin' to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY ./package.json ./
RUN yarn install

COPY . .

# install and add dependencies
EXPOSE 3000
ENTRYPOINT [ "npm", "start" ]
